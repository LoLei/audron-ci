# CI Templates
CI files to be included.
```
include:
  - project: 'cocainefarm/templates/ci'
    file: '/docker.yml'
```

## Docker
Using kaniko to provide dockerless builds.

`docker.yml`: pushes to gitlab internal registry for the project.  
Master and Branches are tagged with their name. `<project>:master` / `<project>:develop`  
Tags are tagged with their version and to latest. `<project>:1.0.0` & `<project>:latest`

`dockerhub.yml`: pushes tags to dockerhub.  
Tags are tagged with their version and to latest. `<project>:1.0.0` & `<project>:latest`

